APPID = "self_discipline"
APPNAME = "自律"
COMMANDS = {
    "自律表格": {
        "commands": ["自律表格", "自律计划表", "我的表现", "本周表现"],
        "action": "/self_discipline/week"
    },
}

# 引入所有鼓励项
from apps.self_discipline.index import _get_options
for (id, option, _) in _get_options():
    COMMANDS.update({
        option: {
            "commands": [option],
            # 需传递参数的动作方法
            "action": "apps.self_discipline.action.encourage:PassParam"
        }
    })

if __name__ == "__main__":
    print(COMMANDS)