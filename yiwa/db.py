# coding: utf8

"""数据库操作，未使用装饰器（数据操作对象传递是个麻烦问题）
或者类处理（目前不是频繁使用，类的得不到及时注销将会常驻内存）"""

import os
import sqlite3
from yiwa.settings import BASE_DIR
from yiwa.log import Log

logger = Log().logger


class Sqlite3DB(object):
    """SQLite操作类"""

    def __init__(self, db_file=os.path.join(BASE_DIR, "yiwa.s3db")):
        super(Sqlite3DB, self).__init__()
        self.db_file = db_file
        self.conn = None
        self.cursor = None

    def connect(self):
        self.conn = sqlite3.connect(self.db_file)
        self.cursor = self.conn.cursor()

    def disconnect(self):
        self.cursor.close()
        self.conn.close()

    def select(self, sql):
        try:
            self.connect()
            self.cursor.execute(sql)
            result = self.cursor.fetchall()
        except Exception as error:
            logger.error(error)
        finally:
            self.disconnect()
            return result

    def execute(self, sql):
        try:
            self.connect()
            self.cursor.execute(sql)
            self.conn.commit()
        except Exception as error:
            logger.error(error)
        finally:
            self.disconnect()

    def executemany(self, sql, data):
        try:
            self.connect()
            self.cursor.executemany(sql, data)
            self.conn.commit()
        except Exception as error:
            logger.error(error)
        finally:
            self.disconnect()


class DataConveyor(object):
    """yiwa数据交互类"""

    def __init__(self):
        super(DataConveyor, self).__init__()
        self.init_captions = {
            0: "请对着我，喊我的名字吧：",
            1: "请对着我，说出指令吧：",
            -1: "很抱歉，我出错了，请重启机器吧。",
        }
        self.s3db = Sqlite3DB()

    def _init(self):
        sql = "SELECT id FROM yiwa"
        if self.s3db.select(sql):
            self.sleep()
        else:
            sql_init_insert = f"""INSERT INTO yiwa(status, caption, listening, info, stt) 
                VALUES(0, "{self.init_captions.get(0)}", 0, "等待唤醒", "")
                """
            self.s3db.execute(sql_init_insert)

    def refresh(self, _apps, _commands):
        self.s3db.execute("DELETE FROM apps")
        self.s3db.execute("DELETE FROM commands")
        self.s3db.execute("UPDATE sqlite_sequence SET seq=0 WHERE name='apps';")
        self.s3db.execute("UPDATE sqlite_sequence SET seq=0 WHERE name='commands';")

        insert_sql_apps = "INSERT INTO apps(appid, appname) VALUES(?,?)"
        self.s3db.executemany(insert_sql_apps, _apps)

        insert_sql_commands = """INSERT INTO commands(name, commands, action, appid) 
            VALUES(?, ?, ?, ?)"""
        self.s3db.executemany(insert_sql_commands, _commands)

    def sleep(self):
        sql = f"""UPDATE yiwa SET status = 0,
            caption="{self.init_captions.get(0)}",
            listening=0,
            info="--! 睡眠待命",
            stt=""
            """
        self.s3db.execute(sql)

    def wakeup(self):
        sql = f"""UPDATE yiwa SET status = 1,
            caption="{self.init_captions.get(1)}",
            info="^_^ 唤醒成功"
            """
        self.s3db.execute(sql)

    def access(self, command):
        sql = f"""UPDATE yiwa SET status = 1,
            caption="{self.init_captions.get(1)}",
            info="指令命中: {command}"
            """
        self.s3db.execute(sql)

    def error(self):
        sql = f"""UPDATE yiwa SET status = -1,
            caption="{self.init_captions.get(-1)}",
            info="接收语音发送错误"
            """
        self.s3db.execute(sql)

    def stt(self, command):
        if not command:
            command = "-"
        sql = f"""UPDATE yiwa SET stt="{command}"
        """
        self.s3db.execute(sql)

    def info(self, info):
        if not info:
            info = "-"
        sql = f"""UPDATE yiwa SET info="{info}"
        """
        self.s3db.execute(sql)

    def listening(self):
        """监听中"""
        sql = f"""UPDATE yiwa SET listening = 1,
            info="录音中"
            """
        self.s3db.execute(sql)

    def listened(self):
        """监听完毕"""
        sql = f"""UPDATE yiwa SET listening = 0,
            info="录音结束"
            """
        self.s3db.execute(sql)

    def all_command(self):
        sql = "SELECT commands, action FROM commands ORDER BY hot DESC"
        return self.s3db.select(sql)

    def filter_command(self, command):
        sql = f"""SELECT commands, action FROM commands 
          WHERE commands LIKE "%{command}%" 
          ORDER BY hot DESC"""
        return self.s3db.select(sql)

    def filter_command_by_actions(self, actions):
        if not actions:
            return []
        in_actions = "','".join(actions)
        sql = f"""SELECT commands, action FROM commands 
          WHERE action IN ('{in_actions}') 
          ORDER BY hot DESC"""
        return self.s3db.select(sql)

    def hot(self, action):
        """增加动作的热度"""
        sql = f"""UPDATE commands SET hot=hot+1 
            WHERE action="{action}"
            """
        self.s3db.execute(sql)
